
# 준비사항

## 라이브러리 위치 지정

    export LD_LIBRARY_PATH=$PWD/lib



# C 예제 빌드 및 실행

    cd test
    make
    ./measure-test



# JNI 예제 빌드 및 실행

    cd jni
    make
    make run

