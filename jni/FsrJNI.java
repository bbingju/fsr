public class FsrJNI {

    static {
        System.loadLibrary("fsrjni");
    }

    private native void init();
    private native void deinit();
    private native void startMeasure();
    private native void stopMeasure();

    public static void main(String[] args) {

        FsrJNI fsr = new FsrJNI();
        
        fsr.init();

        fsr.startMeasure();

        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            // System.out.println(e.getMessate());
        }

        fsr.stopMeasure();

        fsr.deinit();
    }
}

