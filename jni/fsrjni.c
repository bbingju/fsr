#include <jni.h>
#include <stdio.h>
#include "fsr.h"
#include "FsrJNI.h"

static fsr_t *handle;

static void sensor_measured(fsr_measure_data_t *d)
{
    for (int i = 0; i < 8; i++) {
        printf("%u: %u %u %u\n", i + 1,
               d->values[i][0],
               d->values[i][1],
               d->values[i][2]);
    }

    putchar('\n');
}

JNIEXPORT void JNICALL Java_FsrJNI_init(JNIEnv *env, jobject thiz)
{
    fsr_init(&handle, sensor_measured);
}

JNIEXPORT void JNICALL Java_FsrJNI_deinit(JNIEnv *env, jobject thiz)
{
    fsr_deinit(handle);
}

JNIEXPORT void JNICALL Java_FsrJNI_startMeasure(JNIEnv *env, jobject thiz)
{
    fsr_start_measure(handle);
}

JNIEXPORT void JNICALL Java_FsrJNI_stopMeasure(JNIEnv *env, jobject thiz)
{
    fsr_stop_measure(handle);
}
