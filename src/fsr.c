#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <wiringPi.h>
#include <wiringPiI2C.h>
#include <ads1115.h>

#include "../include/fsr.h"

#define PIN_BASE_1 200
#define PIN_BASE_2 300

struct fsr {
    volatile int is_measuring;
    struct fsr_measure_data raw;
    measured_cb_t measured_cb;
    pthread_t tid;
};

static void mux_sel(int sel_num)
{
    switch (sel_num) {
    case 0:
        digitalWrite(1, LOW);
        digitalWrite(0, LOW);
        break;
    case 1:
        digitalWrite(1, LOW);
        digitalWrite(0, HIGH);
        break;
    case 2:
        digitalWrite(1, HIGH);
        digitalWrite(0, LOW);
        break;
    default:
        digitalWrite(1, LOW);
        digitalWrite(0, LOW);
        break;
    }
}

static void read_pad_values(fsr_t *obj)
{
    fsr_measure_data_t *d = &obj->raw;

    for (int padnum = 0; padnum < 4; padnum++) {
        for (int i = 0; i < 3; i++) {
            mux_sel(i);
            usleep(50);
            d->values[padnum][i] = analogRead(PIN_BASE_1 + padnum);
        }
    }

    for (int padnum = 4; padnum < 8; padnum++) {
        for (int i = 0; i < 3; i++) {
            mux_sel(i);
            usleep(50);
            d->values[padnum][i] = analogRead(PIN_BASE_2 + padnum - 4);
        }
    }

}

static void *measure_loop(void *args)
{
    fsr_t *obj = args;

    while (1) {
        if (obj->is_measuring == 0) {
            continue;
        }

        read_pad_values(obj);

        if (obj) {
            obj->measured_cb(&obj->raw);
        }
    }

    return NULL;
}


static void hw_init()
{
    wiringPiSetup();

    pinMode(0, OUTPUT);
    pinMode(1, OUTPUT);

    ads1115Setup(PIN_BASE_1, 0x48);
    ads1115Setup(PIN_BASE_2, 0x49);
}

fsr_err_t fsr_init(fsr_t **obj, measured_cb_t cb)
{
    fsr_t *inst = NULL;

    if (!obj)
        return FSR_ERR_UNDEFINED;

    inst = calloc(1, sizeof (struct fsr));
    if (!inst) {
        return FSR_ERR_UNDEFINED;
    }

    hw_init();

    if (cb)
        inst->measured_cb = cb;

    *obj = inst;

    pthread_create(&inst->tid, NULL, measure_loop, inst);

    return FSR_ERR_NONE;
}

void fsr_deinit(fsr_t *obj)
{
    if (obj) {
        pthread_detach(obj->tid);
        free(obj);
    }
}


fsr_err_t fsr_start_measure(fsr_t *obj)
{
    if (!obj)
        return FSR_ERR_UNDEFINED;

    if (obj->is_measuring) {
        return FSR_ERR_ALREADY_STARTED;
    }

    obj->is_measuring = 1;

    return FSR_ERR_NONE;
}

fsr_err_t fsr_stop_measure (fsr_t *obj)
{
    if (!obj)
        return FSR_ERR_UNDEFINED;

    if (obj->is_measuring == 0) {
        return FSR_ERR_ALREADY_STOPPED;
    }

    obj->is_measuring = 0;

    return FSR_ERR_NONE;
}
