#ifndef FSR_H
#define FSR_H

#include <pthread.h>

typedef enum {
    FSR_ERR_ALREADY_STOPPED = -3,
    FSR_ERR_ALREADY_STARTED = -2,
    FSR_ERR_UNDEFINED       = -1,
    FSR_ERR_NONE            = 0,
} fsr_err_t;

typedef struct fsr_measure_data {
    int values[8][3];
} fsr_measure_data_t;

typedef void (*measured_cb_t) (struct fsr_measure_data *data);

typedef struct fsr fsr_t;

#ifdef __cplusplus
extern "C" {
#endif

    fsr_err_t fsr_init(fsr_t **obj, measured_cb_t cb);
    void fsr_deinit(fsr_t *obj);
    fsr_err_t fsr_start_measure(fsr_t *obj);
    fsr_err_t fsr_stop_measure (fsr_t *obj);

#ifdef __cplusplus
}
#endif

#endif /* FSR_H */
