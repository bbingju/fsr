#include <stdio.h>
#include <unistd.h>
#include <errno.h>

#include "fsr.h"

static fsr_t *handle = NULL;

static void sensor_measured(fsr_measure_data_t *d)
{
    for (int i = 0; i < 8; i++) {
        printf("%u: %u %u %u\n", i + 1,
               d->values[i][0],
               d->values[i][1],
               d->values[i][2]);
    }

    putchar('\n');
}

int main()
{
    fsr_init(&handle, sensor_measured);

    fsr_start_measure(handle);

    sleep(5);

    fsr_stop_measure(handle);

    sleep(3);

    fsr_start_measure(handle);

    sleep(5);

    fsr_deinit(handle);

    return 0;
}
